<?php

// isset= est défini (si la variable est défini alors...) isset($_POST)
// empty= est vide (si $_POST est vide alors...)
// !empty= n'est pas vide

//initialisation
$question_1="";
$resultat="";
$mots_ok = ["Y","YES", "yes", "oui","OUI"];
$Q_1 = isset($_POST['Q_1']) ? strtoupper($_POST['Q_1']) : "";

// verifier si la réponse à la question 1 est correct
if (in_array($Q_1, $mots_ok)) {
    $resultat = "TRUE";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>quizz</title>
    <link rel="stylesheet" href="quizz.css" type="text/css">
</head>
<body>
    <script src="quizz.js"></script>
    <button class="btn" onclick="changeBackground()">CLIQUEZ ICI</button>
    <div class="snow">
        <div class="snow__layer"></div>
        <div class="snow__layer"></div>
        <div class="snow__layer"></div>
    </div>
    <div class="instructions">
        <p>
            Instructions: Ecrivez votre réponse et appuyer sur "entrée"
        </p>
    </div>
    <div class="typewriter-container">
        <div class="typewriter">
        <form action="quizz.php" method="POST">
        <label for="Objet">Le 14 juillet existe il en Allemagne ?</label>
        <input type="text" id="question_1" class="champ_form" name="Q_1" autocomplete="off" placeholder="">
        <input type="submit" value="OK">
        <br>
        <!-- montrer la deuxieme question si reponse juste -->
        <?php if ($resultat == "TRUE") { ?>
            <!-- bonne réponse alors -->
            <label for="Objet">Le médecin vous prescrit 3 pilules à prendre à intervalle d'une demi-heure entre chacune.<br> Combien de temps mettrez-vous pour ingurgiter les 3 pilules ?</label>
            <input type="text" id="question_2" class="champ_form" name="Q_2" autocomplete="off" required placeholder="">
        <?php } else { ?>
            <!-- mauvaise réponse alors reste bloqué à la Q_1 -->
            <div class="error">
                <p>Mauvaise réponse</p>
            </div>

        <?php } ?>
        
        
        <!-- <label for="Objet">Combien font 100 : 0,2 ?</label>
        <input type="text" id="question_3" class="champ_form" name="Q_3" required placeholder="">
        <br>
        <br>
        <label for="Objet">
Un escargot grimpe le long d'un mur de 20 mètres de hauteur à la vitesse de 5 mètres par jour, mais la nuit il redescend de 4 mètres, au bout de combien de temps atteindra-t-il le sommet du mur ?</label>
        <input type="text" id="question_4" class="champ_form" name="Q_4" required placeholder="">
        <br> -->
        </form>
        </div>
    </div>
</body>
</html>

