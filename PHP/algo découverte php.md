# Activité algorithmie avec un langage

PHP :
- ✅ Vérifier que l’install de wampp existe et est fonctionnelle.
- ✅ Démarrer avec un nouveau projet « Apprentissage PHP »
- ✅découverte de quelques fonctions de base : 
    - ✅echo
    - ✅déclaration de variable
        - ✅activité simple : afficher bonjour prenom.
    - ✅conditions
        - ✅activité : si prénom == « le vôtre », alors Bonjour, créateur ! Sinon Bonjour prénom
    - ✅déclaration d’une variable tableau [10 prénoms]
        - ✅activité : si prénom existe dans mon tableau, alors bonjour prénom, sinon bonjour bel inconnu
    - ✅boucle while et for 
        - ✅activité : pour chaque prénom de mon tableau, dire bonjour prénom
        - ✅activité : tant qu’il y a un prénom dans mon tableau, dire bonjour prenom
        - ✅activité : tant que i < 5 , dire bonjour prénom n°[ i ]. S’arrêter après le 5e.
        - ✅activité : stopper la boucle quand j’arrive au 6e prenom. A faire avec for
- ✅Reprise de l’algorithme du zoo :
    - ✅chaque animal doit manger son aliment : simuler le repas en écrivant une phrase

## Activité en autonomie :
Afficher des infos liées à la température sur un mois :
- Construire un tableau, avec une quantité de pluie en mm, une température max et une température min par jour.
- Afficher la température moyenne des températures du mois.
- Afficher la température la plus basse, et la plus haute.
- Afficher un diagramme avec une colonne pour chaque jour, dont la hauteur représente la quantité d’eau tombée chaque jour.
    • Ajouter à ce diagramme un point/jour pour la température haute en rouge, et un point pour la température basse en bleu.
