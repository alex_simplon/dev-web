

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>niveau_2</title>

    <!-- css --> 

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
    <link rel="stylesheet" href="assets/css/style.css">
 <!-- css fin--> 


</head>
<body>

   <div class="container text-center">
    <div class="row justify-content">
      <div class="col col-lg-2">
        <p class="h5">niveau 2<p>
      </div>
    </div>
  </div>

<br>
<br>
<p class="h5">progression<p>

<div class="progress" role="progressbar" aria-label="Basic example" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
  <div class="progress-bar w-0">
  </div>  
</div>

<br>
<br>
<!-- <div id="n2q1" style="display:none"> -->
<p class="h5">niveau_2_Question_1 <p>
<p class="h1 text-center">Complètes la phrase suivante :  Alberto Tomba la ?</p>
  <form action="traitement_niveau_2.php" method="POST">
<div class="text-center">


<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_2_question_1" id="niv2_q1" value="1">
  <label class="form-check-label" for="inlineRadio1">Bomba</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_2_question_1" id="niv2_q1" value="-1">
  <label class="form-check-label" for="inlineRadio2">Cata!</label>
</div>

<br>
<br>
<br>
<!-- <div id="n2q1" style="display:none"> -->
<p class="h5">niveau_2_Question_2 <p>
<p class="h1 text-center">Quelle station de ski accueil le Kandahar?</p>
 <div class="text-center">

<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_2_question_2" id="niv2_q2" value="1">
  <label class="form-check-label" for="inlineRadio1">Les Houches</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_2_question_2" id="niv2_q2" value="-1">
  <label class="form-check-label" for="inlineRadio2">Chamrousse!</label>
</div>
<!-- <div> -->


<br>
<br>
<br>
<!-- <div id="n2q3" style="display:none"> -->
<p class="h5">niveau_2_Question_3 <p>
<p class="h1 text-center">Quelle autre station de ski Française accueille la coupe du monde de ski?</p>

<div class="text-center">

<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_2_question_3" id="niv2_q3" value="1">
  <label class="form-check-label" for="inlineRadio1">Val d'Isère</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_2_question_3" id="niv2_q3" value="-1">
  <label class="form-check-label" for="inlineRadio2">Verbier!</label>



</div>
<!-- <div> -->


       
<br>
<br>
<br>
<button class="btn btn-secondary" type="submit">Traitement php</button>
<br>
<br>


  
  <!-- JavaScript --> 
  <script src="niveau_2.js"></script>
<!-- JavaScript--> 
</body>
</html>