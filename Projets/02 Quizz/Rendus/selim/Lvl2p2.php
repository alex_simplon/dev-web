<?php
session_start();
$tabQuestion=[
  "Quel commande de triche vous fais obtenir 50.000 simflouz dans les sims ?",
  "Quand a été crée Pac-Man ?",
  "Quelle est le Boss du Nether dans Minecraft ?",
  "Quel jeu vidéo fait apparaître Donkey Kong pour la première fois ?",
  "Quel société a développé World of Warcraft :",
  "Comment s'apelle l'héroïne de The Last of Us :",
  "Quel est le jeu le plus speedruné ?"];
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css\quiz.css">
  <title>Document</title>
</head>
<body>
 <div class="parent">

<!-- HEADER -->

    <div class="div1">
<!-- LOGO  -->
            <img class="logoNav"src="images\logopixaxe.png" alt="">
<!-- TITRE HEADER -->
            <h1 class="titreNav">QUIZZ CULTURE JEUX VIDEOS</h1>
<!-- BOUTON RESTART-->
             <a href="index.php" class="restartButton"><b>RESTART</b></a>

    </div>

<!-- TITRE DU LEVEL -->

    <div class="div2">
        <h2 class="titreLevel">Level 2</h2>
        </div>

<!-- C'EST LE TABLEAU -->

    <div class="div3">
        <div class="div3_1">
            <img class="bgTableau"src="images\BG__lvl2.png" alt="image d'arrière plan de streetfighter">

            <h2 class="alertPhone"><img class="imgAlertPhone" src="images\alertePhone.png" alt=""><br><br>Veuillez mettre votre téléphone au format horizontale pour profiter au mieux du quiz.</h2>
        </div>
      <img class="barreLifeR"src="images\barre_life_<?php echo "".$_SESSION['scoreA']."" ?>_lvl2.png" alt="">
      <img class="barreLifeL"src="images\barre_life_<?php echo "".$_SESSION['scoreE']."" ?>.png" alt="">
      <img class="perso1" src="images\perso1_lvl1.gif" alt="Ryu">
      <img class="perso2" src="images\perso2_lvl2.gif" alt="M.bison" style="transform: scaleX(-1); height:60%;" >

    </div>

<!-- QUESTIONS -->

    <div class="div4">
      <?php echo "<p class=\"question\">Question ".$_SESSION['x']." : ".$tabQuestion[$_SESSION['x']-1]."</p>";
       ?>
    </div>

<!-- BOUTONS REPONSES -->

        <?php

          echo "<div class=\"div5\">

          ".$_SESSION['reponse']."

        </div>";

        ?>

    <!-- BOUTON VALIDATION -->

        <div class="div6">

              <?php
                  if ($_SESSION['scoreA']==5) {

                    echo "<form action=\"2intermediairelvl2.php\" method=\"post\">
                  <input type=\"submit\" name=\"valideButton\" class=\"valideButton\" id=\"boutonValidation\" value=\"Niveau suivant\">

                  </form>";

                  }elseif($_SESSION['scoreE']==3) {

                    echo "<form action=\"index.php\" method=\"post\">

                  <input type=\"submit\" name=\"valideButton\" class=\"valideButton\" id=\"boutonValidation\" value=\"RETRY\">";

                  }else{
                    echo"<form action=\"2intermediairelvl2.php\" method=\"post\">

                  <input type=\"submit\" name=\"valideButton\" class=\"valideButton\" id=\"boutonValidation\" value=\"Question suivante\">

                  </form>";
                  }

                ?>



        </div>

</div>
</body>
<script src="js\quiz.js" defer></script>
</html>
