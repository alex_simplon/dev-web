Le projet final est le dernier dossier QUIZ avec les differentes pages + le dossier src.
Il y a eu 2 projets avortés mais j'ai décidé de les laisser pour de futures améliorations.

Pour commencer le quiz lancer la page quiz.php (QUIZZ/quizz/quiz/quiz.php)

C'est un quiz sur le cinéma.
Les questions ont toutes été inventées par moi même, les extraits de films et certaines images ont été travaillés sur Adobe Premiere et Photoshop.
J'ai essayé de garder à l'esprit une certaine logique et d'intégrer un mélange d'humour et de scènes cultes du cinéma.


Journal de bord:

Début du projet: première version avortée car trop portée sur le front-end et premières difficultés en PHP et JS pour mettre en place certaines fonctionnalités comme un bouton qui permet de switcher d'un background à un autre (un peu comme un Dark mode / Light mode)

21 décembre: test du nouveau projet en JS avec l'aide d'une vidéo youtube pour avoir une idée du rendu final. Copier coller tout en essayant de comprendre...

22 déc: test d'intégration PHP mais grosses difficultés

23 déc: abandon du projet test car trop compliqué pour mon niveau actuel, je suis parti sur un QCM tout simple en me concentrant uniquement sur le PHP et si tout fonctionne je ferai l'intégration css, js

26,27 déc: le PHP fonctionne (système de score + résultat), j'ai quand même eu quelques problèmes comme par exemple l'appel de la variable $score++; suivi de $_SESSION['score'] = $score; je pensais que du coup $score++ incrémenterai aussi la $_SESSION['score'] = $score; mais en fait non. Du coup j'ai juste appeler directement $_SESSION['score']++; et ça a fonctionné. J'ai fais pas mal de consoleLog pour résoudre les problèmes, de belles soirées de frustration !

28 déc: fin de la mise en place de toutes les pages + réponse correcte ou non + résultat final en additionnant branche 1 et 2, début front end.

29,30 déc: front end

3 déc: intégration JS

Conclusion: je suis assez satisfait du résultat même s'il y a quand même des choses qui me chagrinent car mon code n'est pas DRY, j'aurai aimé avoir plus de temps pour mettre toutes mes questions en une seule page et utiliser JS pour afficher une question à la fois. J'aurai aimé mettre en place également un tableau des scores avec le nom renseigné du joueur en début de quiz qui se met à jour dès que quelqu'un renseigne un nouveau pseudo ou bien qu'il fasse un meilleur score dans le cas où c'est le même joueur. En rajoutant une variable "chrono" pour que chaque score soit différent et pas juste des 20/20 etc, par exemple 15 bonnes réponses + chrono = 30 secondes, alors points = 2500. 
Il manque également le responsive. 
Et il me semble que lorsque l'on inspecte le code on ne peut pas voir le code PHP donc on ne sait pas quelles réponses sont justes ou fausses pour les petits mâlins.

