<?php
session_start();

// les differents echo possible en fonction du nombre de bonnes réponses en fonction de la valeur de $score
if ($_SESSION['score'] === 10) {
    echo '<p class="pp1">Bravo vous êtes vraiment calé ! Le cinéma n'."'".' a aucun secret pour vous.<br> Votre score est de 10/10 !</p>';
} elseif ($_SESSION['score'] === 9) {
    echo '<p class="pp1">Vous êtes doué ! Votre score est de 9/10 !</p>';
} elseif ($_SESSION['score'] === 8) {
    echo '<p class="pp1">Vous êtes doué ! Votre score est de 8/10 !</p>';
} elseif ($_SESSION['score'] === 7) {
    echo '<p class="pp1">Vous y étiez presque ! Votre score est de 7/10 !</p>';
} elseif ($_SESSION['score'] === 6) {
    echo '<p class="pp1">Pas mal. Votre score est de 6/10 !</p>';
} elseif ($_SESSION['score'] === 5) {
    echo '<p class="pp1">Juste la moyenne. Votre score est de 5/10 !</p>';
} elseif ($_SESSION['score'] === 4) {
    echo '<p class="pp1">Révisez vos classiques. Votre score est de 4/10 !</p>';
} elseif ($_SESSION['score'] === 3) {
    echo '<p class="pp1">Vous aimez le cinéma ? Votre score est de 3/10 !</p>';
} elseif ($_SESSION['score'] === 2) {
    echo '<p class="pp1">Dommage. Votre score est de 2/10 !</p>';
} elseif ($_SESSION['score'] === 1) {
    echo '<p class="pp1">Faite un effort... Votre score est de 1/10 !</p>';
} elseif ($_SESSION['score'] === 0) {
    echo '<p class="pp1">Sans commentaire. Votre score est de 0/10 !</p>';
}
if (isset($_POST['input'])) {
    if ($_POST['input'] === 'NIVEAU SUPERIEUR') {
        header('Location: cinemas1.php');
        exit;
    } elseif ($_POST['input'] === 'NIVEAU INFERIEUR') {
        header('Location: cinemai1.php');
        exit;
    }
}
// petite condition en plus qui permet d'afficher un message différent en fonction du score
if ($_SESSION['score'] > 5) {
    echo '<p class="pp1">Vous pouvez choisir un niveau plus élevé mais vous avez le choix</p>';
} elseif ($_SESSION['score'] <=5) {
    echo '<p class="pp1">Vous pouvez choisir un niveau moins élevé mais vous avez le choix</p>';
}
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="p1">
    <img class="pop" src="src/pop.png">
    <form method="post">
        <input class="cinemaccueil" type="submit" name="input" value="NIVEAU SUPERIEUR">
        <input class="cinemaccueil" type="submit" name="input" value="NIVEAU INFERIEUR">
    </form>
</body>
</html>

