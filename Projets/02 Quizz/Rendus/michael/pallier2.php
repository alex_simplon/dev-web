<?php
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="palliers.css">
    <title>Pallier 2</title>
</head>
<body>
        <form action="functions2.php" method="POST" name="form">

            <div class="parent1" id="p2_q1">
                <div class="question" class="parent1" id="p2_q1">
                    <p class="P_Q">Pallier 2 | Question 1/4</p>
                    <p>Comment appelle-t-on l’application des méthodes de Machine Learning à une grande base de données?</p>
                </div>
                    
                    <input type="checkbox" name="q1" class="submit1" id="1a" value="1a" onclick='hide("p2_q1"); show("p2_q2")'>
                    <label for="1a">A. Big data</label>
                    
                    <input type="checkbox" name="q1" class="submit2" id="1b" value="1b" onclick='hide("p2_q1"); show("p2_q2")'>
                    <label for="1b">B. Data mining</label>
                    
                    <input type="checkbox" name="q1" class="submit3" id="1c" value="1c" onclick='hide("p2_q1"); show("p2_q2")'>
                    <label for="1c">C. Intelligence artificielle</label>
                    
                    <input type="checkbox" name="q1" class="submit4" id="1d" value="1d" onclick='hide("p2_q1"); show("p2_q2")'>
                    <label for="1d">D. Internet des objets</label>
            </div>
            
            <div class="parent2" id="p2_q2">
                <div class="question">
                    <p class="P_Q">Pallier 2 | Question 2/4</p>
                    <p>Les algorithmes de Machine Learning construisent un modèle à partir d’un ensemble de données, connues sous le nom de ___________</p>
                </div>
                    <input type="checkbox" name="q2" value="2a" class="submit1" id="2a" onclick='hide("p2_q2"); show("p2_q3")'>
                    <label for="2a">A. Données d’apprentissage</label>
                
                    <input type="checkbox" name="q2" value="2b" class="submit2" id="2b" onclick='hide("p2_q2"); show("p2_q3")'>
                    <label for="2b">B. Transfert de données</label>
                
                    <input type="checkbox" name="q2" value="2c" class="submit3" id="2c" onclick='hide("p2_q2"); show("p2_q3")'>
                    <label for="2c">C. Formation des données</label>
                
                    <input type="checkbox" name="q2" value="2d" class="submit4" id="2d" onclick='hide("p2_q2"); show("p2_q3")'>
                    <label for="2d">D. Aucune de ces réponses</label>
            </div>

            <div class="parent3" id="p2_q3">
                <div class="question">
                    <p class="P_Q">Pallier 2 | Question 3/4</p>
                    <p>Le Machine Learning est un sous-ensemble de _____________</p>
                </div>
                <input type="checkbox" name="3a" value="q3" class="submit1" value="OUI" onclick='hide("p2_q3"); show("p2_q4")' id="3a">
                    <label for="3a">A. Big data</label>
                
                    <input type="checkbox" name="q3" value="3b" class="submit2" id="3b" onclick='hide("p2_q3"); show("p2_q4")'>
                    <label for="3b">B. Data mining</label>
                
                    <input type="checkbox" name="q3" value="3c" class="submit3" id="3c" onclick='hide("p2_q3"); show("p2_q4")'>
                    <label for="3c">C. Deep Learning</label>
                
                    <input type="checkbox" name="q3" value="3d" class="submit4" id="3d" onclick='hide("p2_q3"); show("p2_q4")'>
                    <label for="3d">D. Intelligence artificielle</label>
            </div>

            <div class="parent4" id="p2_q4">
                <div class="question">
                    <p class="P_Q">Pallier 2 | Question 4/4</p>
                    <p>8. Qui est le père du Machine Learning ?</p>
                </div>
                    <input type="checkbox" name="q4" value="4a" class="submit1" id="4a" onclick='hide("p2_q4"); show("next-button")'>
                    <label for="4a">A. Gregoit Hill</label>
                
                    <input type="checkbox" name="q4" value="4b" class="submit2" id="4b" onclick='hide("p2_q4"); show("next-button")'>
                    <label for="4b">B. Geoffrey Hinson</label>
                
                    <input type="checkbox" name="q4" value="4c" class="submit3" id="4c" onclick='hide("p2_q4"); show("next-button")'>
                    <label for="4c">C. Geoffrey Everest Hinton</label>
                
                    <input type="checkbox" name="q4" value="4d" class="submit4" id="4d" onclick='hide("p2_q4"); show("next-button")'>
                    <label for="4d">D. Geoffrey Ever</label>
                </div>
                
                <div class="box_next">
                    <input type="submit" class="next" id="next-button" value="Passer au pallier suivant">
                </div>
        </form>
            
            <script src="script.js"></script>
</body>
</html>

<?php
?>