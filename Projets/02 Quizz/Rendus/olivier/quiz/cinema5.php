<?php
session_start();



if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Bruce Wayne') {
        header('Location: cinema6.php');
        exit;
    } else if ($_POST['input'] === 'Jack Napier') {
        header('Location: cinema6.php');
        exit;
    } else if ($_POST['input'] === 'Arthur Fleck') {
        $_SESSION['score']++;
        header('Location: cinema6.php');
        exit;
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="Q4">
    <img class="pop" src="src/pop.png">
    <video id="ecran" autoplay controls>
        <source src="src/joker.mp4" type="video/mp4">
    </video>
    <p class="txt2">Comment s'appelle le Joker dans le film "Joker" sorti en 2019 ?</p>
    <form method="post">
        <input class="cinema1" type="submit" name="input" value="Jack Napier">
        <input class="cinema1" type="submit" name="input" value="Arthur Fleck">
        <input class="cinema1" type="submit" name="input" value="Bruce Wayne">
    </form>
</body>
</html>