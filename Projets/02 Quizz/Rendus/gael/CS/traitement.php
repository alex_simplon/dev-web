<?php
require "BDD.php";

// Fonction pour donner 1pt par bonne réponse en comparant avec le tableau des réponses : int.
function corrections (array $reponses_U): int {
    $points = 0;
    for ($k=0; $k < count($reponses_U); $k++) {
        if (in_array($reponses_U[$k],REPONSES)) {
            $points += 1;
        }
    }
    return $points;
}


//Function pour récuperer le $_GET/POST si la valeur a été définie.
function is_set (array $_GetOuPost, string $k) {
    if (isset($_GetOuPost["$k"])) {
        return ($_GetOuPost["$k"]);
    }
}

?>