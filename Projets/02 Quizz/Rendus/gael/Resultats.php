<?php
session_start();
$pseudo = htmlentities($_SESSION["pseudo"]);
$reponses1 = $_SESSION["reponses1"];
$reponses2 = $_SESSION["reponses2"];
$reponses3 = $_SESSION["reponses3"];
require "Structures/header.php";
?>

<?php
$resultat = corrections($reponses1) + corrections($reponses2) + corrections($reponses3);
if ($resultat === 15 ) {
    echo "Magnifique $pseudo, vous avez eu $resultat/15 !!! Vous êtes un futur développeur en devenir !!!";
    } else if ($resultat >= 8) {
        echo "Bravo $pseudo vous avez eu $resultat/15, vous êtes au dessus de la moyenne !!!";
        } else {
            echo "Désolé $pseudo vous avez eu $resultat/15, vous ferez certainement mieux la prochaine fois.";
    }
?>

<?php
require "Structures/footer.php";
?>