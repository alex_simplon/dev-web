<?php
session_start();


if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Arnold Schwarzenegger') {
        header('Location: cinema9.php');
        exit;
    } else if ($_POST['input'] === 'Jack Nicholson') {
        $_SESSION['score']++;
        header('Location: cinema9.php');
        exit;
    } else if ($_POST['input'] === 'Bruce Willis') {
        header('Location: cinema9.php');
        exit;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="pj">
    <img class="pop" src="src/pop.png">
    <p class="pj4">Qui est cet acteur ?</p>
    <form method="post">
        <input class="cinemajn" type="submit" name="input" value="Arnold Schwarzenegger">
        <input class="cinemajn" type="submit" name="input" value="Jack Nicholson">
        <input class="cinemajn" type="submit" name="input" value="Bruce Willis">
    </form>
</body>
</html>