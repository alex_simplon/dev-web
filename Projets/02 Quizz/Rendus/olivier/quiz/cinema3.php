<?php
session_start();




if (isset($_POST['input'])) {
    if ($_POST['input'] === 'LES EXPLORATEURS') {
        header('Location: cinema4.php');
        exit;
    } else if ($_POST['input'] === 'LE GRAND BLEU') {
        header('Location: cinema4.php');
        exit;
    } else if ($_POST['input'] === 'LES VISITEURS') {
        $_SESSION['score']++;
        header('Location: cinema4.php');
        exit;
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>Document</title>
</head>
<body id="corn" class="Q3">
    <img class="pop" src="src/pop.png">
    <p class="pq3">De quel film est tirée cette image ?</p>
    <form method="post">
        <input class="cinema3" type="submit" name="input" value="LES EXPLORATEURS">
        <input class="cinema3" type="submit" name="input" value="LE GRAND BLEU">
        <input class="cinema3" type="submit" name="input" value="LES VISITEURS">
    </form>
</body>
</html>