
// Niveau 1
// Les questions

const questions = [
    {
        question: "Comment dit-on ' Triste ' en anglais?.",
        optionA: "Fine",
        optionB: "Angry",
        optionC: "Sad",
        optionD: "Cool",
        correctOption: "optionC"
    },

    {
        question: "Comment dit-on ' Cheval ' en anglais?.",
        optionA: "Fox",
        optionB: "Horse",
        optionC: "Bird",
        optionD: "Dog",
        correctOption: "optionB"
    },

    {
        question: "Comment dit-on ' Frites ' en anglais?.",
        optionA: "Snack",
        optionB: "Frites",
        optionC: "Crunch",
        optionD: "Chips",
        correctOption: "optionD"
    },

    {
        question: "Comment dit-on ' Lundi ' en anglais?.",
        optionA: "Monday",
        optionB: "Tuesday",
        optionC: "Thursday",
        optionD: "Sunday",
        correctOption: "optionA"
    },

    {
        question: "Comment dit-on ' Bonsoir ' en angalis?.",
        optionA: "Good morning",
        optionB: "Good luck",
        optionC: "Good evening",
        optionD: "Goodbye",
        correctOption: "optionC"
    },

    {
        question: "En anglais, ' Spring ' signifie:",
        optionA: "Hiver",
        optionB: "Printemps",
        optionC: "Eté",
        optionD: "Automne",
        correctOption: "optionB"
    },

    {
        question: "Traduis: ' Have a nice Weekend!'.",
        optionA: "Passe un bon week-end!",
        optionB: "Passe une bonne semaine!",
        optionC: "Passe un bon week-end à Nice!",
        optionD: "J'ai passé un bon week-end!",
        correctOption: "optionA"
    },

    {
        question: "Que désigne ' The mouth ' en anglais?.",
        optionA: "La souris",
        optionB: "le sourire",
        optionC: "La bouche",
        optionD: "La mouche",
        correctOption: "optionC"
    },

    {
        question: "Comment dit-on ' Arbre ' en anglias?.",
        optionA: "Three",
        optionB: "Tree",
        optionC: "Free",
        optionD: "Arber",
        correctOption: "optionB"
    },

    {
        question: "Traduis en français: ' The son '.",
        optionA: "Le fils",
        optionB: "Le soleil",
        optionC: "Le son",
        optionD: "Le frére",
        correctOption: "optionA"
    },



    // Niveau 2:

    {
        question: "They ...... my prarents.",
        optionA: "Is",
        optionB: "Be",
        optionC: "Are",
        optionD: "Being",
        correctOption: "optionC"
    },


    {
        question: "She works ....... saturday.",
        optionA: "On",
        optionB: "At",
        optionC: "To",
        optionD: "IN",
        correctOption: "optionA"
    },

    {
        question: "My brother is ....... than me.",
        optionA: "Tallest",
        optionB: "More tall",
        optionC: "More big",
        optionD: "Taller",
        correctOption: "optionD"
    },

   

    {
        question: "Where ......... you come from?.",
        optionA: "Do",
        optionB: "Are",
        optionC: "In",
        optionD: "IS",
        correctOption: "optionB"
    },

    {
        question: "My lesson is ....... three o'clock.",
        optionA: "At",
        optionB: "On",
        optionC: "For",
        optionD: "In",
        correctOption: "optionA"
    },

   

    {
        question: "I have a .......",
        optionA: "Car red",
        optionB: "Car colour red",
        optionC: "Redding car",
        optionD: "Red car",
        correctOption: "optionD"
    },

    {
        question: "Traduis: ' Oublier ' en anglais.",
        optionA: "Forgive",
        optionB: "Forget",
        optionC: "Remember",
        optionD: "Oubly",
        correctOption: "optionB"
    },

    {
        question: "En anglais ' A pair of trousers ' désigne:",
        optionA: "Une trousse",
        optionB: "Un pull en laine",
        optionC: "Une paire de ciseaux",
        optionD: "Un pantalon",
        correctOption: "optionD"
    },

    {
        question: "Comment traduis-tu ' Une chemise ' en anglais?.",
        optionA: "A short",
        optionB: "A skirt",
        optionC: "A shirt",
        optionD: "A tie",
        correctOption: "optionC"
    },

    {
        question: "Comment dit-on ' Dehors ' en anglais?.",
        optionA: "Outside",
        optionB: "Upside",
        optionC: "Downside",
        optionD: "Inside",
        correctOption: "optionA"
    },

// Niveaux 3:
    {
        question: "Si quelqu'un te dit ' A storm is coming ' tu ferais mieux de rentrer chez toi car, on vient de t'annoncer l'arrivée.",
        optionA: "D'un monstre",
        optionB: "D'une canicule",
        optionC: "D'une invasion de vers de terre",
        optionD: "D'une têmpete",
        correctOption: "optionD"
    },


    {
        question: "Sandre ...... scary movies.",
        optionA: "Doesn't like",
        optionB: "Don't like",
        optionC: "Isn't likes",
        optionD: "Likes not",
        correctOption: "optionA"
    },


    {
        question: "We ...... at the beach yesterday.",
        optionA: "Did be",
        optionB: "Were",
        optionC: "Was",
        optionD: "In",
        correctOption: "optionB"
    },


    {
        question: "Their house is ..... on the street.",
        optionA: "More big",
        optionB: "More bigger ",
        optionC: "The biggest",
        optionD: "Bigger",
        correctOption: "optionC"
    },


    {
        question: "Fred is a vegetarian. He ...... meat.",
        optionA: "Sometimes eats",
        optionB: "Often eats",
        optionC: "Usually eats",
        optionD: "Never eats",
        correctOption: "optionD"
    },
    {
        question: "Comment dit-on ' Dehors ' en anglais?.",
        optionA: "Outside",
        optionB: "Upside",
        optionC: "Downside",
        optionD: "Inside",
        correctOption: "optionA"
    },
    
    {
        question: "Can I have ...... milk please?.",
        optionA: "No",
        optionB: "Any",
        optionC: "Some",
        optionD: "A",
        correctOption: "optionC"
    },

    {
        question: "You ......... pay extra for the  food. It's included.",
        optionA: "Have to",
        optionB: "Don't need to",
        optionC: "Don't have",
        optionD: "Doesn't have to",
        correctOption: "optionB"
    },

    {
        question: "I ..... to Tokyo before.",
        optionA: "Have been ",
        optionB: "Been",
        optionC: "Has been",
        optionD: "having been",
        correctOption: "optionA"
    },


    {
        question: "They .......... shopping every Tuesday.",
        optionA: "Is",
        optionB: "Go",
        optionC: "Going",
        optionD: "Goes",
        correctOption: "optionB"
    }







]


let shuffledQuestions = [] //Tableau vide pour contenir les questions sélectionnées mélangées

function handleQuestions() { 
    //fonction pour mélanger et passer 30 questions vers le tableau shuffledQuestions
    while (shuffledQuestions.length <= 9) {
        const random = questions[Math.floor(Math.random() * questions.length)]
        if (!shuffledQuestions.includes(random)) {
            shuffledQuestions.push(random)
        }
    }
}


let questionNumber = 1
let playerScore = 0  
let wrongAttempt = 0 
let indexNumber = 0

// Affichage de la question suivante dans le tableau vers DOM
function NextQuestion(index) {
    handleQuestions()
    const currentQuestion = shuffledQuestions[index]
    document.getElementById("question-number").innerHTML = questionNumber
    document.getElementById("player-score").innerHTML = playerScore
    document.getElementById("display-question").innerHTML = currentQuestion.question;
    document.getElementById("option-one-label").innerHTML = currentQuestion.optionA;
    document.getElementById("option-two-label").innerHTML = currentQuestion.optionB;
    document.getElementById("option-three-label").innerHTML = currentQuestion.optionC;
    document.getElementById("option-four-label").innerHTML = currentQuestion.optionD;

}


function checkForAnswer() {
    const currentQuestion = shuffledQuestions[indexNumber] //Pour obtient la question actuelle
    const currentQuestionAnswer = currentQuestion.correctOption //Obtient la réponse actuelle de la question
    const options = document.getElementsByName("option"); //Obtient tous les éléments dans DOM avec le nom 'option' (radio, inputs)  
    let correctOption = null

    options.forEach((option) => {
        if (option.value === currentQuestionAnswer) {
            // Obtient le bon radio input avec la bonne réponse
            correctOption = option.labels[0].id
        }
    })
   
    // vérification pour etre sure pour le radio, input a été vérifiée ou qu’une option a été choisie
    if (options[0].checked === false && options[1].checked === false && options[2].checked === false && options[3].checked == false) {
        document.getElementById('option-modal').style.display = "flex"
    }

    //Vérifier si le bouton radio  est identique à la réponse
    options.forEach((option) => {
        if (option.checked === true && option.value === currentQuestionAnswer) {
            document.getElementById(correctOption).style.backgroundColor = "green"
            playerScore++
            indexNumber++
            //Configuré pour retarder le number de la question jusqu’au chargement de la question suivante
            setTimeout(() => {
                questionNumber++
            }, 1000)
        }

        else if (option.checked && option.value !== currentQuestionAnswer) {
            const wrongLabelId = option.labels[0].id
            document.getElementById(wrongLabelId).style.backgroundColor = "red"
            document.getElementById(correctOption).style.backgroundColor = "green"
            wrongAttempt++
            indexNumber++
            //Configuré pour retarder le number de la question jusqu’au chargement de la question suivante
            setTimeout(() => {
                questionNumber++
            }, 1000)
        }
    })
}



//appelé lorsque le bouton suivant est appelé
function handleNextQuestion() {
    checkForAnswer()
    unCheckRadioButtons()
    //retarde l’affichage de la question suivante pendant une seconde
    setTimeout(() => {
        if (indexNumber <= 9) {
            NextQuestion(indexNumber)
        }
        else {
            handleEndGame()
        }
        resetOptionBackground()
    }, 1000);
}

//Remet la valeur null aux options après l’affichage des bonnes ou mauvaises couleurs
function resetOptionBackground() {
    const options = document.getElementsByName("option");
    options.forEach((option) => {
        document.getElementById(option.labels[0].id).style.backgroundColor = ""
    })
}

// Décochez tous les boutons radio pour la question suivante (peut être fait avec la boucle foreach aussi)
function unCheckRadioButtons() {
    const options = document.getElementsByName("option");
    for (let i = 0; i < options.length; i++) {
        options[i].checked = false;
    }
}

// fonction pour quand toutes les questions sont répondues
function handleEndGame() {
    let remark = null
    let remarkColor = null

    // Vérification de l’état du joueur et de la couleur 
    if (playerScore <= 3) {
        remark = "Mauvaises notes, Continuez à pratiquer."
        remarkColor = "red"
    }
    else if (playerScore >= 4 && playerScore < 7) {
        remark = "Ne vous décourager pas, Vous pouvez faire mieux!."
        remarkColor = "orange"
    }
    else if (playerScore >= 7) {
        remark = "Excellent, Continuez votre bon travail."
        remarkColor = "green"
    }
    const playerGrade = (playerScore / 10) * 100

    //Données à afficher sur le tableau d’affichage
    document.getElementById('remarks').innerHTML = remark
    document.getElementById('remarks').style.color = remarkColor
    document.getElementById('grade-percentage').innerHTML = playerGrade
    document.getElementById('wrong-answers').innerHTML = wrongAttempt
    document.getElementById('right-answers').innerHTML = playerScore
    document.getElementById('score-modal').style.display = "flex"

}

//Ferme le score modal et réinitialise le jeu
function closeScoreModal() {
    questionNumber = 1
    playerScore = 0
    wrongAttempt = 0
    indexNumber = 0
    shuffledQuestions = []
    NextQuestion(indexNumber)
    document.getElementById('score-modal').style.display = "none"
}

//Fonction pour fermer le modal d’avertissement
function closeOptionModal() {
    document.getElementById('option-modal').style.display = "none"
}