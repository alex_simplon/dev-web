<?php
session_start();
$pseudo = ucfirst(htmlentities($_SESSION["pseudo"]));
require "Structures/header.php";
?>

<main class="bgQ1">
    <h1>Bienvenue <?= $pseudo ?>, nous allons jouer à un jeu...</h1>
    <p> Il peut avoir une ou plusieurs réponses pour une seule question</p>
    <form action="#"  method="GET">    
        <?php form($Q1) ?>
        <input class="btn" name="reponses1" type="submit" value="Valider">
    </form>
</main>

<?php
$_SESSION["reponses1"] = is_set($_GET, "reponses_U");
if(isset($_GET["reponses1"])) {
    header("location:Q2.php");
}
require "Structures/footer.php" ?>


