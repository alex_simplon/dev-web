<?php
include ('config.php');
$score=0;
// fonction qui vérifie qu'un pseudo a été entré et le rentre dna les variables de session 
// laisse l'utilisateur entrer sur la page du formulaire si le pseudo est validé
//sinon affiche une demande de saisir le pseudo
function validationPseudo(){
    if (isset($_POST['pseudo'])){
        $pseudo = $_POST['pseudo'];
        $_SESSION['username'] = $pseudo;
        $_SESSION['connect'] = true;
        $_SESSION['score'] = 0;
        header('location:niveau1.php');
         exit();
    }
    else if (!isset($_POST['pseudo']) && isset($_POST['validerPseudo'])){echo "Veuillez saisir un pseudo";}
}


function verifConnection() {
    //  Si la personne n'est pas connectée, on la renvoie sur la page d'authentification.
    if (!$_SESSION['connect']) {
    header("location:index.php");
    exit();
    }
    // On vérifie si on a cliqué sur le bouton "Se Déconnecter" :
    if (isset($_POST['sortir'])) {
    // Si oui, on supprime la session :
    session_destroy();
        echo "session destroy";
    // Puis on redirige vers la page d'authentification
    header("location:index.php");
    exit();
    }
}


function calcul_score($question, $reponse_juste ){
    $score = $_SESSION['score'];
    //une réponse a été sélectionnée et c'est la réponse juste 
    if (isset($_POST[$question])&&($_POST[$question] == $reponse_juste)){
        $score +=5;
        return $score; }
    else if(isset($_POST[$question])){return $score; }
  

}