<?php
session_start();
$reponses1 = $_SESSION["reponses1"];
$reponses2 = $_SESSION["reponses2"];
$pseudo = ucfirst(htmlentities($_SESSION["pseudo"]));
require "Structures/header.php";
?>

<main class="bgQ3">
    <h1> <?= $pseudo ?> ton score actuel est de : <?= corrections($reponses1) + corrections($reponses2) ?>/9 </h1>
    <form action="#"  method="GET">
        <?php form($Q3) ?>
        <input class="btn" name="reponses3" type="submit" value="Valider">
    </form>
</main>

<?php
$_SESSION["reponses3"] = is_set($_GET, "reponses_U");
if(isset($_GET["reponses3"])) {
    header("location:Resultats.php");
}
require "Structures/footer.php" ?>
