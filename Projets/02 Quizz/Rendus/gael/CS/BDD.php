<?php

$Q1 = [
    "Quelle balise est utilisée pour mettre un titre en HTML ?" => ["h", "p", "a", "titre"],
    "Quelle balise sert à sauter une ligne en HTML ? " => ["n", "br", "u", "i"],
    "Quelle balise sert à mettre un lien via href en HTML ?" => ["src", "a", "input", "link"],
    "Quelle balise sert à mettre une image en HTML ?" => ["picture", "jpeg", "img", "svg"]
];



$Q2=[
    "Quelle est l'expression pour changer le fond d'écran en CSS " => ["background", "background-image", "background-color", "src"],
    "Quelle est l'expression pour changer la police en CSS ?" => ["font-size", "font-family", "font-weight", "font-style"],
    "Quelle est l'expression pour changer la hauteur d'un élément en CSS ?" => ["width", "top", "height", "max-height"],
    "Quel choix a-t-on pour l'attribut 'display' en CSS ? " => ["none", "height", "flex", "px"]
];

$Q3=[
    "Quelle super variable utilise-t-on pour démarrer ou reprendre une session en PHP ? " => ["start_session()", "session-start()", "begin_session()", "new_session()"],
    "Que va-t-on utiliser pour 'afficher' une fonction en PHP ?" => ["echo", "< ?=", "console.log()", "return" ],
    "Quelle super variable va-t-on utiliser pour passer à une autre page en PHP ?" => ["action", "href", "header", "src"],
    "Quelles sont les boucles utilisables en PHP ? " => ["Foreach", "while", "if", "repeat"]
];

 define("REPONSES", [
    "h",
    "br",
    "a",
    "img",
    "background-color",
    "font-family",
    "height",
    "none",
    "flex",
    "session-start()",
    "echo",
    "< ?=",
    "header",
    "Foreach",
    "while"
]);


?>