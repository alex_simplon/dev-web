const quizData = [
    {
        question: "Dans quel pays s'est déroulée la Coupe du Monde 2010 ?",
        a:"Senegal",
        b:"Afrique du sud",
        c:"France",
        d:"Brésil",
        correct:"b",
    },
    {
        question: "Quelle equipe a gagner le plus coupe du monde ?",
        a:"Brésil",
        b:"Italie",
        c:"Espagne",
        d:"Argentine",
        correct:"a",
    },
    {
        question: "Qui est le meilleur buteur de la coupe du monde 2022 ?",
        a:"Messi",
        b:"Cristiano Ronaldo",
        c:"Neymar",
        d:"Mbappe",
        correct:"d",
    },
    {
        question: "Qui est le seul Français à avoir remporté la Coupe du monde en tant que joueur et aussi en tant que sélectionneur ?", 
        a:"Just Fontaine",
        b:"Petit",
        c:"Zidane",
        d:"Didier Descamps",
        correct: "d",
    },
    {
        question: "Quel est le surnom donné à Lionel Messi ?",
        a: "La pulga",
        b: "El gato",
        c: "Leo",
        d: "La joya",
        correct: "a",
    },
    {
        question: "Dans quel equipe Cristiano Ronaldo n'a pas t-il joue ?",
        a: "Juventus",
        b: "Grenoble",
        c: "Real madrid",
        d: "sporting Lisbon",
        correct: "b",
    },
    {
        question: "Qui a remporter la coupe du monde 2018 en Russie?",
        a: "Olande",
        b: "Mali",
        c: "France",
        d: "Argentine",
        correct: "c",
    },
    {
        question: "Qui parmis ces joueurs a remporte le plus de Ballon d'or ?",
        a: "Zidane",
        b: "Cristiano Ronaldo",
        c: "Maradona",
        d: "Messi",
        correct: "d",
    },
    {
        question: "Qui est le joueur le mieux payé ?",
        a: "Messi",
        b: "Mbappe",
        c: "Neymar",
        d: "Giroud",
        correct: "b",
    },
    {
        question: "Quel est nom du stade de Paris Saint-Germain ?",
        a: "Parc des Princes",
        b: "Maracana",
        c: "Stade de Paris",
        d: "Velodrome",
        correct: "a",
    },

];



const answerEls = document.querySelectorAll('.answer')
const questionEl= document.getElementById('question')

const a_text = document.getElementById('a_text')
const b_text = document.getElementById('b_text')
const c_text = document.getElementById('c_text')
const d_text = document.getElementById('d_text')

// const img = document.getElementById ('img')

const submitBtn = document.getElementById('submit')


let currentQuiz = 0
let score = 0





loadQuiz()
function loadQuiz () {

    deselectAnswer()

    const currentQuizData = quizData[currentQuiz]

    questionEl.innerText = currentQuizData.question
    a_text.innerText = currentQuizData.a
    b_text.innerText = currentQuizData.b
    c_text.innerText = currentQuizData.c
    d_text.innerText = currentQuizData.d
   
    
}


function deselectAnswer() {
    answerEls.forEach(answerEl => answerEl.checked = false)
}

function getSelected(){
    let answer
    answerEls.forEach(answerEl => {
        if(answerEl.checked) {
            answer = answerEl.id
        }
    })
    return answer
}

submitBtn.addEventListener('click', () => {
    const answer = getSelected()
    if(answer) {
        if(answer === quizData[currentQuiz].correct) {
            score++
        }
        
        currentQuiz++

        if(currentQuiz < quizData.length) {
            loadQuiz()
        }else{
            quiz.innerHTML = `
            <h2>VOTRE SCORE: ${score}/${quizData.length}<h2>

            <button onclick="location.reload()">Recommencez!</button>
            `
        }
    }


})