Le Quiz Géographie 
(Questionnaire à Choix Multiples (QCM))

1. Il s'agit d'un simple quiz en HTML5, CSS3, JavaScript et PHP

2. Le quiz commence avec la page index.php

3. Il y a 3 * 5 questions et quatre réponses par question

4. Chaque page contient 5 questions sur les quelles on repond on cliquant sur la réponse qu'on a choisi

5. Après chaque question, il faut cliquer sur le bouton qui indique "Prochaine Question n°"

6. Avec la cinqième question, un button, qui demande de confirmer les cinq questions, apparâit et le bouton "Prochaine Question n°" disparâitra  

7. Les points de chaque page (5 questions) va être afficher sur une page externe et le bouton "Cliquez ICI" donne accès aux prochaines cinq questions

8. La confirmation des cinq dernières questions amène sur la dernière page sur laquelle les résultats intermédiaires et le résultat global vont être afficher

9. Le boutton "Start Quiz >> Cliquez ICI" amène sur la page index.php où on peut décider de recommencer à jouer 
