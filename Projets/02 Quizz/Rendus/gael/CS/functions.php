<?php

// Utilisation d'une super variable avec la fonction "sanitize" pour changer automatiquement le titre de chaque page, : int
$title = sanitize($_SERVER["SCRIPT_NAME"]);


// Fonction pour enlever les caractères superflus et ne laisser que le titre de la page, : int
function sanitize(string $lien): string {
    $mots_bannis = [".php","/","_","projet-quizz-par-palier", "Projet","Quizz"];
    $resultat = str_replace($mots_bannis, "", $lien);
    return $resultat;
}


// Fonction pour afficher automatiquement les questions selon la page sur laquelle se trouve l'utilisateur 
function form (array $title) {
    foreach($title as $questions => $reponses) {
        echo <<<HTML
        <div class="$reponses[0]" id="$reponses[0]">
            <h2 class="titreQ" id="titreQ">  $questions  </h2>
HTML;
        foreach($reponses as $reponse) {
            echo <<<HTML
                <label for="reponses_U">$reponse</label>
                <input type="checkbox" name="reponses_U[]" value="$reponse">
HTML;
        }
        echo <<<HTML
            <button type="button" id="$reponse" class="bt $reponses[1]">Prochaine Question </button>
        </div>
HTML;


    }
}
 ?>
