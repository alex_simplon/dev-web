<?php
session_start();
$tabQuestion=[
  "Quel commande de triche vous fais obtenir 50.000 simflouz dans les sims ?",
  "Quand a été crée Pac-Man ?",
  "Quelle est le Boss du Nether dans Minecraft ?",
  "Quel jeu vidéo fait apparaître Donkey Kong pour la première fois ?",
  "Quel société a développé World of Warcraft :",
  "Comment s'apelle l'héroïne de The Last of Us :",
  "Quel est le jeu le plus speedruné ?"];
  $tabReponseA = ["Rosebud","1983","EnderDragon","DK Country","Blizzard","Abby","Super Mario 64"];
  $tabReponseB = ["GiveMeMoney","1980","Wither","DK 64","Ubisoft","Ellie","Dark Souls"];
  $tabReponseC = ["Motherlode","1991","Notch","DK Arcade","Gameloft","Jessie","The Legends of Zelda"];
  $tabreponselvl1 =["c","b","b","c","a","b","a"];
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css\quiz.css">
  <title>Document</title>
</head>
<body>
 <div class="parent">

<!-- HEADER -->

    <div class="div1">
<!-- LOGO  -->
            <img class="logoNav"src="images\logopixaxe.png" alt="">
<!-- TITRE HEADER -->
            <h1 class="titreNav">QUIZZ CULTURE JEUX VIDEOS</h1>
<!-- BOUTON RESTART-->
              <a href="index.php" class="restartButton"><b>RESTART</b></a>

    </div>

<!-- TITRE DU LEVEL -->

    <div class="div2">
        <h2 class="titreLevel">Level 2</h2>
        </div>

<!-- C'EST LE TABLEAU -->

    <div class="div3">
        <div class="div3_1">
            <img class="bgTableau"src="images\BG__lvl2.png" alt="image d'arrière plan de streetfighter">

            <h2 class="alertPhone"><img class="imgAlertPhone" src="images\alertePhone.png" alt=""><br><br>Veuillez mettre votre téléphone au format horizontale pour profiter au mieux du quiz.</h2>
        </div>
      <img class="barreLifeR"src="images\barre_life_<?php echo "".$_SESSION['scoreA']."" ?>_lvl2.png" alt="">
      <img class="barreLifeL"src="images\barre_life_<?php echo "".$_SESSION['scoreE']."" ?>.png" alt="">
      <img class="perso1" src="images\perso1_lvl1.gif" alt="ryu">
      <img class="perso2" src="images\perso2_lvl2.gif" alt="M.bison" style="transform: scaleX(-1); height:60%;">

    </div>

<!-- QUESTIONS -->

    <div class="div4">
      <?php
      echo "<p class=\"question\">Question ".$_SESSION['x']." : ".$tabQuestion[$_SESSION['x']-1]."</p>";

       ?>
    </div>

<!-- BOUTONS REPONSES -->

        <div class="div5">

          <form action="1intermediairelvl2.php" method="post">

              <input type="radio" name="boutonreponse" class="boutonreponse" id="button-a" value="a">
              <label for="button-a" id="buttonA"> <?php echo"A&nbsp;:&nbsp;".$tabReponseA[$_SESSION['x']-1]."" ?> </label>

              <input type="radio" name="boutonreponse" class="boutonreponse" id="button-b" value="b">
              <label for="button-b" id="buttonB" > <?php echo"B&nbsp;:&nbsp;".$tabReponseB[$_SESSION['x']-1]."" ?> </label>

              <input type="radio" name="boutonreponse" class="boutonreponse" id="button-c" value="c">
              <label for="button-c" id="buttonC"><?php echo"C&nbsp;:&nbsp;".$tabReponseC[$_SESSION['x']-1]."" ?></label>

        </div>

    <!-- BOUTON VALIDATION -->

        <div class="div6">

              <input type="submit" name="valideButton" class="valideButton" id="boutonValidation" value="VALIDER">

            </form>

        </div>

</div>
</body>
<script src="js\quiz.js" defer></script>
</html>
