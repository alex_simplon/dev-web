<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/svg+xml" />
  <title>Question de 11 à 15</title>
  <link rel="stylesheet" href="./assets/css/main.css" />
</head>

<body>
  <main>
    <div id="container">
      <header>
        <h1>Quiz Géographie</h1>
        <h2>Sur les capitales du&nbsp;monde</h2>
      </header>

      <button onclick="question_11();" class="start_game3" id="start_game3">
        Les questions de 11&nbsp;à&nbsp;15
      </button>
      <button class="btnstyle" id="btnQuest12" onclick="question_12();">
        Prochaine question 12
      </button>
      <button class="btnstyle" id="btnQuest13" onclick="question_13();">
        Prochaine question 13
      </button>
      <button class="btnstyle" id="btnQuest14" onclick="question_14();">
        Prochaine question 14
      </button>
      <button class="btnstyle" id="btnQuest15" onclick="question_15();">
        Prochaine question 15
      </button>

      <form action="result3.php" method="post" id="quiz">
        <ul id="urli">
          <li id="eleventh_li">
            <h3>La capitale du Mexique est<span>...</span></h3>

            <div>
              <input type="radio" name="question-11" id="question-11-A" value="A" checked />
              <label for="question-11-A">A) San José</label>
            </div>

            <div>
              <input type="radio" name="question-11" id="question-11-B" value="B" />
              <label for="question-11-B">B) Tegucigalpa</label>
            </div>

            <div>
              <input type="radio" name="question-11" id="question-11-C" value="C" />
              <label for="question-11-C">C) Mexico</label>
            </div>

            <div>
              <input type="radio" name="question-11" id="question-11-D" value="D" />
              <label for="question-11-D">D) Manille</label>
            </div>
          </li>

          <li id="twelfth_li">
            <h3>La capitale de la Pologne est<span>...</span></h3>

            <div>
              <input type="radio" name="question-12" id="question-12-A" value="A" checked />
              <label for="question-12-A">A) Bucarest</label>
            </div>

            <div>
              <input type="radio" name="question-12" id="question-12-B" value="B" />
              <label for="question-12-B">B) Skopje</label>
            </div>

            <div>
              <input type="radio" name="question-12" id="question-12-C" value="C" />
              <label for="question-12-C">C) Budapest</label>
            </div>

            <div>
              <input type="radio" name="question-12" id="question-12-D" value="D" />
              <label for="question-12-D">D) Varsovie</label>
            </div>
          </li>

          <li id="thirteenth_li">
            <h3>La capitale d'Oman est<span>...</span></h3>

            <div>
              <input type="radio" name="question-13" id="question-13-A" value="A" checked />
              <label for="question-13-A">A) Mascate</label>
            </div>

            <div>
              <input type="radio" name="question-13" id="question-13-B" value="B" />
              <label for="question-13-B">B) Abuja</label>
            </div>

            <div>
              <input type="radio" name="question-13" id="question-13-C" value="C" />
              <label for="question-13-C">C) Khartoum</label>
            </div>

            <div>
              <input type="radio" name="question-13" id="question-13-D" value="D" />
              <label for="question-13-D">D) N’Djamena</label>
            </div>
          </li>

          <li id="fourteenth_li">
            <h3>La capitale de Kenya est<span>...</span></h3>

            <div>
              <input type="radio" name="question-14" id="question-14-A" value="A" checked />
              <label for="question-14-A">A) Kigali</label>
            </div>

            <div>
              <input type="radio" name="question-14" id="question-14-B" value="B" />
              <label for="question-14-B">B) Nairobi</label>
            </div>

            <div>
              <input type="radio" name="question-14" id="question-14-C" value="C" />
              <label for="question-14-C">C) Mogadiscio</label>
            </div>

            <div>
              <input type="radio" name="question-14" id="question-14-D" value="D" />
              <label for="question-14-D">D) Dakar</label>
            </div>
          </li>

          <li id="fifteenth_li">
            <h3>La capitale d'Uruguay est<span>...</span></h3>

            <div>
              <input type="radio" name="question-15" id="question-15-A" value="A" checked />
              <label for="question-15-A">A) Panama</label>
            </div>

            <div>
              <input type="radio" name="question-15" id="question-15-B" value="B" />
              <label for="question-15-B">B) San Salvador</label>
            </div>

            <div>
              <input type="radio" name="question-15" id="question-15-C" value="C" />
              <label for="question-15-C">C) Caracas</label>
            </div>

            <div>
              <input type="radio" name="question-15" id="question-15-D" value="D" />
              <label for="question-15-D">D) Montevideo</label>
            </div>
          </li>
        </ul>

        <input type="submit" value="Confirmez" id="SUBMIT" />
      </form>
    </div>
  </main>
  <script src="./assets/js/script3.js"></script>
</body>

</html>