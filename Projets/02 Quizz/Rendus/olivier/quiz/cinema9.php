<?php
session_start();


if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Çi') {
        header('Location: cinema10.php');
        exit;
    } else if ($_POST['input'] === 'Ça') {
        $_SESSION['score']++;
        header('Location: cinema10.php');
        exit;
    } else if ($_POST['input'] === 'Cela') {
        header('Location: cinema10.php');
        exit;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="pc">
    <img class="pop" src="src/pop.png">
    <p class="pca">Dans quel film apparaît ce personnage ?</p>
    <form method="post">
        <input class="cinema4" type="submit" name="input" value="Çi">
        <input class="cinema4" type="submit" name="input" value="Ça">
        <input class="cinema4" type="submit" name="input" value="Cela">
    </form>
</body>
</html>