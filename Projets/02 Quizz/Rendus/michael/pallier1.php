<?php

?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="palliers.css">
    <title>Pallier 1</title>
</head>
<body>
        <form action="functions1.php" method="POST" name="form">

            <div class="parent1" id="p1_q1">
                <div class="question" class="parent1" id="p1_q1">
                    <p class="P_Q">Pallier 1 | Question 1/4</p>
                    <p>Completez les trous suivants :</p>
                    <p>Le machine learning est l'acquisition ____ de connaissances par l'utilisation de programmes ____</p>
                </div>
                    
                    <input type="checkbox" name="q1" class="submit1" id="1a" value="1a" onclick='hide("p1_q1"); show("p1_q2")'>
                    <label for="1a">A. selective / manuels</label>
                    
                    <input type="checkbox" name="q1" class="submit2" id="1b" value="1b" onclick='hide("p1_q1"); show("p1_q2")'>
                    <label for="1b">B. Selective / informatique</label>
                    
                    <input type="checkbox" name="q1" class="submit3" id="1c" value="1c" onclick='hide("p1_q1"); show("p1_q2")'>
                    <label for="1c">C. Autonome / Manuels</label>
                    
                    <input type="checkbox" name="q1" class="submit4" id="1d" value="1d" onclick='hide("p1_q1"); show("p1_q2")'>
                    <label for="1d">D.<br>Autonomes / Informatiques</label>
            </div>
            
            <div class="parent2" id="p1_q2">
                <div class="question">
                    <p class="P_Q">Pallier 1 | Question 2/4</p>
                    <p>2 - Qu'est-ce que le Machine Learning ?</p>
                </div>
                    <input type="checkbox" name="q2" value="2a" class="submit1" value="" id="2a" onclick='hide("p1_q2"); show("p1_q3")'>
                    <label for="2a">A. Le Machine Learning (ML) est un domaine de l’informatique.</label>
                
                    <input type="checkbox" name="q2" value="2b" class="submit2" id="2b" onclick='hide("p1_q2"); show("p1_q3")'>
                    <label for="2b">B. Le Machine Learning est un type d’intelligence artificielle qui permet d’extraire des modèles à partir de données brutes en utilisant un algorithme ou une méthode.</label>
                
                    <input type="checkbox" name="q2" value="2c" class="submit3" id="2c" onclick='hide("p1_q2"); show("p1_q3")'>
                    <label for="2c">C. L’objectif principal du Machine Learning est de permettre aux systèmes informatiques d’apprendre à partir de l’expérience sans être explicitement programmés ou sans intervention humaine.</label>
                
                    <input type="checkbox" name="q2" value="2d" class="submit4" id="2d" onclick='hide("p1_q2"); show("p1_q3")'>
                    <label for="2d">D. Toutes les réponses sont vraies</label>
            </div>

            <div class="parent3" id="p1_q3">
                <div class="question">
                    <p class="P_Q">Pallier 1 | Question 3/4</p>
                    <p>3. Le Machine Learning est un domaine de l’intelligence artificielle consistant en des algorithmes d’apprentissage qui _______</p>
                </div>
                <input type="checkbox" name="3a" value="q3" class="submit1" value="OUI" onclick='hide("p1_q3"); show("p1_q4")' id="3a">
                    <label for="3a">A. Améliorent leurs performances</label>
                
                    <input type="checkbox" name="q3" value="3b" class="submit2" id="3b" onclick='hide("p1_q3"); show("p1_q4")'>
                    <label for="3b">B. S’améliore à force d’exécution d’une tâche</label>
                
                    <input type="checkbox" name="q3" value="3c" class="submit3" id="3c" onclick='hide("p1_q3"); show("p1_q4")'>
                    <label for="3c">C. s’améliorent avec le temps et l’expérience</label>
                
                    <input type="checkbox" name="q3" value="3d" class="submit4" id="3d" onclick='hide("p1_q3"); show("p1_q4")'>
                    <label for="3d">D. Toutes les réponses sont vraies</label>
            </div>

            <div class="parent4" id="p1_q4">
                <div class="question">
                    <p class="P_Q">Pallier 1 | Question 4/4</p>
                    <p>4. Identifiez le type d'algorithme d'apprentissage pour les « identités du visage pour les expressions faciales ».</p>
                </div>
                    <input type="checkbox" name="q4" value="4a" class="submit1" id="4a" onclick='hide("p1_q4"); show("next-button")'>
                    <label for="4a">A. Prédiction</label>
                
                    <input type="checkbox" name="q4" value="4b" class="submit2" id="4b" onclick='hide("p1_q4"); show("next-button")'>
                    <label for="4b">B. Schémas de reconnaissance</label>
                
                    <input type="checkbox" name="q4" value="4c" class="submit3" id="4c" onclick='hide("p1_q4"); show("next-button")'>
                    <label for="4c">C. Reconnaissance d’anomalies</label>
                
                    <input type="checkbox" name="q4" value="4d" class="submit4" id="4d" onclick='hide("p1_q4"); show("next-button")'>
                    <label for="4d">D. Génération de motifs</label>
                </div>
                
                <div class="box_next">
                    <input type="submit" class="next" id="next-button" value="Passer au pallier suivant">
                </div>
        </form>
            
            <script src="script.js"></script>
</body>
</html>
