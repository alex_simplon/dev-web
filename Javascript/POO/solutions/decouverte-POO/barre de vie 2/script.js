// Créer une classe Barre de vie, qu'on pourra appeler autant de fois qu'on veut.
class BarreDeVie {
  _vie; // stockera les points de vie entre 0 et 100
  _couleur; // stockera la couleur de la barre en fonction des points de vie
  _id; // stockera l'ID aléatoire de l'objet instancié
  _nom; // stockera le nom de l'objet instancié

  // Si on nous passe les paramètres vie et couleur, on les utilise, sinon on prend les paramètres par défaut.
  constructor(nom,vie = 100,couleur = "green"){
    if (nom) {
      this._nom = nom;
    }else{
      // Exemple d'erreur personnalisée : throw permet d'écrire du texte lors d'une erreur, et de stopper l'execution du code.
      throw("L'objet a besoin d'un nom");
    }
    // On vérifie aussi que vie est comprise entre 0 et 100.
    if (vie <= 100 && vie >= 0) {
      this._vie = vie;
      this._couleur = couleur;
    }else{
      throw('la vie doit être comprise entre 0 et 100')
    }
    this._id = this.idAleatoire(0, 100000000000);

    // On récupère les éléments HTML
    let affichageBarresDeVie = document.querySelector('#affichageBarresDeVie');

    // On crée notre élément dans le HTML
    // on appelle une méthode de notre classe, en faisant comme
    // avec les variables : this.methode()
    this.creerBarre(this._vie,this._couleur,this._id,this._nom);
  }

  // getter qui permet d'afficher la vie.
  get vie(){
    return this._vie;
  }

  // Getter qui permet de récupérer l'objet conteneur de l'objet en cours.
  get conteneur(){
    let conteneur = document.querySelector('#conteneur'+this._id);
    return conteneur;
  }
  // Permet de récupérer l'objet barredevie dans le html de l'objet en cours.
  get barredevie(){
    let barredevie = document.querySelector('#barredevie'+this._id);
    return barredevie;
  }

  // Setter qui permet de rajouter de la vie.
  // Si la vie actuelle plus la vie ajoutée est inférieur à 100, on additionne la vie actuelle et la vie ajoutée. si elle dépasse 100, on met la vie à 100 (on ne peut pas aller au-dessus).
  // Puis on redessine la barre.
  set gagnerVie(vie){
    if (this._vie + vie <= 100) {
      this._vie += vie;
    }else{
      this._vie = 100;
    }
    this.modifierBarre(this._vie,this._couleur,this._id)
  }

  // Setter sous forme de setter pour enlever de la vie.
  // Fonctionne comme gagnerVie.
  // Dans le cas où on atteint zéro, on appelle la fonction mort.
  set perdreVie(vie){
    if (this._vie - vie > 0) {
      this._vie -= vie;
      this.modifierBarre(this._vie,this._couleur,this._id)
    }else{
      this._vie = 0;
      this.mort(this._id);
    }

  }

  // Méthode qui permet de créer l'élement visuel.
  creerBarre(vie,couleur,id,nom){
    // Dans l'élément général, on crée tous les éléments contenant la barre de vie.
    affichageBarresDeVie.innerHTML += "<div id=\"conteneur"+id+"\"><div id= \"bordure"+id+"\" style=\"width:500px; margin:50px auto; border: 2px solid black;\"><div id=\"barredevie"+id+"\" style=\"background-color:"+couleur+";width:"+vie+"%; height:20px; text-align:center; color:#FFF;\"></div></div></div>"

    // On récupère ensuite ces différents éléments

    // on écrit la vie dans la barre :
    // on appelle la méthode barredevie de la classe, qui renvoie l'élément html.
    this.barredevie.innerHTML = vie+"%";

    // On ajoute deux boutons pour enlever et ajouter de la vie, en faisant appel aux méthodes de l'objet.

    this.conteneur.innerHTML += '<button onclick="'+nom+'.perdreVie = 10">Perdre de la vie</button><button onclick="'+nom+'.gagnerVie = 10">Gagner de la vie</button>'
  }

  // Méthode qui permet de changer les paramètres de la barre de vie :
  // - la taille de la barre,
  // - la couleur,
  // - le pourcentage écrit dans la barre.
  modifierBarre(vie,couleur,id){

    this.barredevie.style.backgroundColor = couleur;
    this.barredevie.style.width = vie+"%";
    this.barredevie.innerHTML = vie+"%";
  }

  //Méthode qui permet, Dans le cas où on meurt, de modifier la barre pour la mettre en rouge, puis d'écrire dedans un petit texte.
  mort(id){
    this.modifierBarre(100,'red',id);
    this.barredevie.innerHTML = "Tu es mort";
  }

  // Petite méthode qui donne un chiffre aléatoire entre deux valeurs.
  idAleatoire(min,max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}


// On instancie ensuite notre classe.
let barre = new BarreDeVie('barre');

