<?php
$score = 0;
if (!empty($_POST)) {
    $reponse1 = $_POST['reponse1'];
    $reponse2 = $_POST['reponse2'];
    $reponse3 = $_POST['reponse3'];
    $reponse4 = $_POST['reponse4'];
    if ($reponse1 == 500) {
        $score++;
    } else {
        echo "<p>Sérieusement ?</p>";
    }
    if ($reponse2 == 4) {
        $score++;
    } else {
        echo "<p>Faites un effort !</p>";
    }
    if ($reponse3 == 12 ) {
        $score++;
    } else {
        echo "<p>Vous avez déjà fais des maths ?</p>";
    }
    if ($reponse4 == 5) {
        $score++;
    } else {
        echo "<p>Bon...</p>";
    }
    echo "<p>Vous avez fais un score de $score /4 !</p>";
    // $grade = ($score / 4) * 100;
    // echo "$grade";
    if ($score == 0) {
        echo "<p>Vous êtes nul...</p>";
    } elseif ($score == 1 ) {
        echo "<p>Même ma grand mère aurait fait mieux</p>";
    } elseif ($score == 2) {
        echo "<p>Vous avez la moyenne</p>";
    } elseif ($score == 3) {
        echo "<p>Vous avez fais une faute, dommage !</p>";
    } elseif ($score == 4) {
        echo "<p>Bien joué !!</p>";
    }
} 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="test2.css" type="text/css">
    <script src="test2.js" defer></script>
    <title>test2</title>
</head>
<body>
    <p>HELLO WORLD QUIZ</p>
    <form class="quiz" method="post" action="test2.php">
        <p>Combien font 100 : 0,2 ?</p>
        <input type="nombre" name="reponse1">
        <p>Combien font 5 - 1 ?</p>
        <input type="nombre" name="reponse2">
        <p>Combien font 6 x 2 ?</p>
        <input type="nombre" name="reponse3">
        <p>Combien font 10 : 2 ?</p>
        <input type="nombre" name="reponse4">
        <br><br>
        <input type="submit">
    </form>
</body>
</html>