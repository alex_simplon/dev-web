<?php
session_start();
// ce sera le même principe sur chaques pages question
// condition: si l'on clique sur PAS alors la variable score s'incrémente et l'on est redirigé vers la question 2
if (isset($_POST['input'])) {
    if ($_POST['input'] === 'PAS') {
        $_SESSION['score']++;
        header('Location: cinema2.php');
        exit;
        // si l'on clique sur JAMAIS alors on est redirigé vers la question 2 et la variable score n'est pas pris en compte
    } else if ($_POST['input'] === 'JAMAIS') {
        header('Location: cinema2.php');
        exit;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="p2">
    <img class="pop" src="src/pop.png">
    <video id="ecran" autoplay controls>
        <source src="src/gandalf.mp4" type="video/mp4">
    </video>
    <p class="txt1">Vous ne passerez ... ?</p>
    <form method="post">
        <input class="cinema1" type="submit" name="input" value="JAMAIS">
        <input class="cinema1" type="submit" name="input" value="PAS">
    </form>
</body>
</html>