<?php
session_start();


if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Jackie Brown') {
        header('Location: cinema7.php');
        exit;
    } else if ($_POST['input'] === 'V pour Vendetta') {
        header('Location: cinema7.php');
        exit;
    } else if ($_POST['input'] === 'Kill Bill') {
        $_SESSION['score']++;
        header('Location: cinema7.php');
        exit;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="pk">
    <img class="pop" src="src/pop.png">
    <p class="txt2">Dans quel film apparaît ce personnage ?</p>
    <form method="post">
        <input class="cinema1" type="submit" name="input" value="Jackie Brown">
        <input class="cinema1" type="submit" name="input" value="V pour Vendetta">
        <input class="cinema1" type="submit" name="input" value="Kill Bill">
    </form>
</body>
</html>