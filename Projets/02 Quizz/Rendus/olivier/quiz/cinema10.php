<?php
session_start();


if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Luc Besson') {
        header('Location: resultatcine.php');
        exit;
    } else if ($_POST['input'] === 'Vincent Cassel') {
        header('Location: resultatcine.php');
        exit;
    } else if ($_POST['input'] === 'Benoit Poelvoorde') {
        $_SESSION['score']++;
        header('Location: resultatcine.php');
        exit;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="p2">
    <img class="pop" src="src/pop.png">
    <video id="ecran" autoplay controls>
        <source src="src/pigeon.mp4" type="video/mp4">
    </video>
    <p class="txtb">Qui est cet acteur ?</p>
    <form method="post">
        <input class="cinemab" type="submit" name="input" value="Luc Besson">
        <input class="cinemab" type="submit" name="input" value="Vincent Cassel">
        <input class="cinemab" type="submit" name="input" value="Benoit Poelvoorde">
    </form>
</body>
</html>