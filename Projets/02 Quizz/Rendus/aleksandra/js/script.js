document.addEventListener('DOMContentLoaded', function() {
    "use strict";

    const quiz = document.querySelector('.quiz-body'); 
    const btnStart = document.querySelector('.btn-start'); 
    const form = document.querySelector('.quiz-form');
    const fieldsetItems = form.querySelectorAll('fieldset');
    const btnsPrev = document.querySelectorAll('.btn-prev');
    const startScreen = document.querySelector('.hidden');
    const btnRestart = document.querySelector('.btn-restart');
    const picBtns = document.querySelectorAll('.choice-first');
    // const inputsBtns = fieldsetItems.querySelectorAll('.btn-quiz');

    // // hiding all the blocks of form
    // quiz.style.display = "none";

    // // showing the block of quiz after clicking the start button
    // btnStart.addEventListener('click', () => {
    //     quiz.style.display = "block";
    // });

    // showing the fieldsets one by one
    fieldsetItems.forEach((fieldsetItem, fieldsetItemIndex) => {
        if(fieldsetItemIndex === 0) {
            fieldsetItem.style.display = "block";
        } else {
            fieldsetItem.style.display = "none";
        }

        if (fieldsetItemIndex !== fieldsetItems.length -1) {
            const inputsBtns = fieldsetItem.querySelectorAll('input');

            inputsBtns.forEach((input) => {
                const parent = input.parentNode;
                input.checked = false;
                parent.classList.remove('btn-quiz');
            });
        }
        // console.log(fieldsetItem);
        fieldsetItem.addEventListener('change', (event) => {
            const target = event.target;
            console.log(target);
        });
    });

    // making the possibility to click the div (which has to be used as a button)
    // and then to go to further questions
    function btnPicIsClicked() {
        console.log('clicked!')
    }
    picBtns.forEach((buttonPic, btnPicIndex) => {
        buttonPic.addEventListener('click', (event) => {
            event.preventDefault(); 
            fieldsetItems[btnPicIndex].style.display = "none";
            fieldsetItems[btnPicIndex + 1].style.display = "block";
        });
    });

    // possibility to go back clicking the button "back"
    btnsPrev.forEach((btn, btnIndex) => {
        btn.addEventListener('click', (event) => {
            event.preventDefault();
            fieldsetItems[btnIndex + 1].style.display = "none";
            fieldsetItems[btnIndex].style.display = "block";

        });
    });

    // inputsBtns.forEach((btn, btnIndex) => {
    //     btn.addEventListener('click', (event) => {
    //         event.preventDefault();
    //         fieldsetItems[btnIndex].style.display = "none";
    //         fieldsetItems[btnIndex + 1].style.display = "block";

    //     });
    // });

    // const postData = (body) => {
    //     // console.log(body);
    //     return fetch('php/server.php', {
    //         method: "POST",
    //         headers: {
    //             "Content-type": "application/json",
    //         },
    //         body: JSON.stringify(body),
    //     });
    // };


    
});