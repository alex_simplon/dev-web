<?php
session_start();
$reponses1 = $_SESSION["reponses1"];
$pseudo = ucfirst(htmlentities($_SESSION["pseudo"]));
require "Structures/header.php";
?>

<main class="bgQ2">
    <h1> <?= $pseudo ?> ton score actuel est de : <?= corrections($reponses1) ?>/4 </h1>
    <form action="#"  method="GET">
        <?php form($Q2) ?>
        <input class="btn" name="reponses2" type="submit" value="Valider">
    </form>
</main>

<?php
$_SESSION["reponses2"] = is_set($_GET, "reponses_U");
if(isset($_GET["reponses2"])) {
    header("location:Q3.php");
}
require "Structures/footer.php" ?>
