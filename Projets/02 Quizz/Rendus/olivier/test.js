const chatBody=document.querySelector(".chat-body");
const txtInput=document.querySelector("#txtInput");
var sauvegarde = document.getElementById('reponse_bonjour');
    sauvegarde.value = txtInput.value;

// permet d'appuyer sur entrée pour envoyer sa réponse
txtInput.addEventListener("keyup", (event) => {
    if (event.keyCode === 13) {
    renderUserMessage();
    
    }
});

const renderUserMessage = () => {
    const userInput = txtInput.value;
    renderMessageEle(userInput, "user");
    
    // pour clear la zone de texte à chaque message
    txtInput.value = "";
    // délai pour affichage de la réponse du bot
    setTimeout(() => {
        renderChatbotResponse(userInput);
        setScrollPosition();
    }, 1000);
};
// pour que le chat réponde
const renderChatbotResponse = (userInput) => {
    const res=getChatbotResponse(userInput);
    renderMessageEle(res);
};

const renderMessageEle=(txt, type) => {
    let className="user-message";
    if (type !== 'user'){
        className = "chatBot-message";
    }
    const messageEle=document.createElement("div");
    const txtNode = document.createTextNode(txt);
    messageEle.classList.add(className);
    messageEle.append(txtNode);
    chatBody.append(messageEle);
};

// pour envoyer une reponse au message ecrit
const getChatbotResponse = (userInput) => {
    return responseObj[userInput]==undefined?"Veuillez essayer autre chose":responseObj[userInput]
};

// pour que l'affichage reste tout le temps en bas de page
const setScrollPosition = () => {
    if (chatBody.scrollHeight > 0 ) {
        chatBody.scrollTop = chatBody.scrollHeight;
    }
}