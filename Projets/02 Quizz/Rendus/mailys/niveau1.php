<?php
session_start();
require 'config.php';
require 'functions.php';
$_SESSION['score']= $score;
include('traitement.php');

verifConnection();

//vérification des réponses  et ajustement du score
//si la reponse est guybrush threepood j'ajoute 5points au score
//$form_valide = verif_reponses('reponseq1', 'btnVal_q1_n1',"q1reponse2" );


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <title>Niveau 1</title>
</head>
<body class="bg-image img_monkey1">
    <!-- en tête de la page -->
    <header class="container-fluid bg-primary p-2 ">
        <h1 class="text-light text-center">Niveau 1 Monkey Island</h1>
        <h2> A toi de jouer <?= $_SESSION['username']; $_SESSION['connect'];?>!</h2>
        <!-- bouton retour page d'accueil -->
        <form method="POST">
         <button type="submit" class="bg-secondary text-light rounded col-xl-1 col-xs-5 " name="sortir" >sortir </button>   
        </form> 
    </header>
       
    <!-- formulaire question réponses à cocher 1 seule réponse possible -->
    <form method="POST">               
        
            <!-- //division question1 -->
        <div class="container col-xl-6 col-sm-9 bg-tercery text-center p-5 m-auto mt-5 rounded-5" id="q1_n1">
            <label for="question1">Quel est le nom du gouverneur de l'île? </label><br>
            <input type="radio" name="reponseq1" id="q1reponse1"  value="q1reponse1" required>
            <label for="q1reponse1">Luc Martini</label><br>
            <input type="radio" name="reponseq1" id="q1reponse2" value="q1reponse2" >
            <label for="q1reponse2">Guybrush Threepwood</label><br>
            <input type="radio" name="reponseq1" id="q1reponse3" value="q1reponse3" >
            <label for="q1reponse3">Monkey head</label><br>
            <input type="radio" name="reponseq1" id="q1reponse4" value="q1reponse4" >
            <label for="q1reponse3">Elaine Marley</label><br>
            <!-- bouton de validation -->
            <div class="bg-primary rounded p-1 mt-4" onclick="changerQuestion('q1_n1', 'q2_n1')"> question suivante</div>
        </div>
        <!-- bloc questionnaire deuxième question du niveau 1 -->
        <div class="container col-xl-6 col-sm-9 bg-tercery text-center p-5 m-auto mt-5 rounded-5" style="display:none" id="q2_n1">
            <!-- formulaire question réponses à cocher 1 seule réponse possible -->

            <label for="question1">Quel est le rêve du personnage principal?</label><br>
            <input type="radio"  name="reponseq2" id="q2reponse1"value="q2reponse1" required>
            <label for="q2reponse1">Faire le tour du monde en bateau.</label><br>
            <input type="radio" name="reponseq2" id="q2reponse2" value="q2reponse2">
            <label for="q2reponse2">devenir le plus grand cuisinier de l'île.</label><br>
            <input type="radio" name="reponseq2" id="q2reponse3" value="q2reponse3">
            <label for="q2reponse3">devenir pirate.</label><br>
            <input type="radio" name="reponseq2" id="q2reponse4" value="q2reponse4">
            <label for="q2reponse3">Pouvoir voler.</label><br>
            <!-- bouton de validation permet de passer a la question suivante avec la fct JS changerQuestion -->
            <div type="button" class="bg-primary rounded p-1 mt-4" onclick="changerQuestion('q2_n1', 'q3_n1')">question suivante </div>
            <!-- bouton retour question precedante -->
            <button type="button" class="bg-secondary text-light mt-2" onclick="questionPrecedante('q2_n1', 'q1_n1')" >retour question précédante </button>
        </div>

        <div class="container col-xl-6 col-sm-9 bg-tercery text-center p-5 m-auto mt-5 rounded-5" style="display:none;" id="q3_n1">
            <!-- formulaire question réponses à cocher 1 seule réponse possible -->          
            <label for="question3">Quel est le nom de l'île?</label><br>
            <input type="radio" name="reponseq3" id="q3reponse1" value="q3reponse1" required>
            <label for="q3reponse1">coconut Island</label><br>
            <input type="radio" name="reponseq3" id="q3reponse2" value="q3reponse2">
            <label for="q3reponse2">Monkey Island</label><br>
            <input type="radio" name="reponseq3" id="q3reponse3" value="q3reponse3">
            <label for="q3reponse3">Guybrush's Island</label><br>
            <input type="radio" name="reponseq3" id="q3reponse4" value="q3reponse4">
            <label for="q3reponse3">île de Mêlée</label><br>
            <input type="submit" value="valider les réponses" name="formulaire1" class="bg-primary rounded p-1 mt-4" id="btn_valide_N1"><br>
            <button type="button" class="bg-secondary text-light mt-2" onclick="questionPrecedante('q3_n1', 'q2_n1')" >retour question précédante </button>

        </div>
    </form>
        <!-- affichage du score -->
        <div class="etoile">
            <div class="content">    
                <h2>score:</h2>
                <p><?= $_SESSION['score']; ?> points</p>
            </div>
        </div>
    
<script src="assets/script/javascript.js"> </script>
    
</body>
</html>