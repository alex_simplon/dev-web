# Projet Quizz
Créer une application de type quizz/QCM/questionnaire sur le thème de votre choix (en gardant toujours en tête que
cette application pourrait être consultée par un·e employeur·se).


## Fonctionnalités obligatoire

- Affichage des questions et choix de la réponse
- Enchaînement de plusieurs questions sur un même niveau (affichage au fur et à mesure des réponses : on ne voit qu'une question à la fois)
- Traitement des réponses lors du changement de page du questionnaire (il en comportera au moins 3). Les Réponses seront analysées côté serveur.
- Lors de ces traitements intermédiaires, on ajuste la phase suivante : questions en fonction des réponses, changement de niveau de difficulté, palier,  
- Affichage de la réponse finale et/ou Décompte des points et affichage du score (en temps réel, et/ou à la fin)
- Responsive

Ne pas hésiter à rajouter d'autres fonctionnalités une fois celles ci implémentées (timer, plusieurs réponses possibles, réponse en champ de texte, illustrations pour certaines questions, affichage des questions à la fin, en vert si la réponse donnée était bonne, en rouge sinon, etc.)

### Compétences mobilisées

Dans l'idéal, vous devrez utiliser des variables, conditions, tableaux, boucles et fonctions. Manipulation du DOM (querySelector / Events).
Essayez de maintenir votre [code DRY](https://bruno-orsier.developpez.com/principes/dry/).
Utilisez le bon langage en fonction de ce que vous voulez faire : traitement côté serveur : PHP, animation du questionnaire : javascript, Mise en forme : html & css.

## Réalisation

Commencez par trouver le thème de votre quizz et une liste de questions pour celui ci. (Interdiction de recopier l'exemple !)
Ensuite réalisez une ou plusieurs maquettes fonctionnelles de ce à quoi ressemblera l'application.
Vous devrez rendre un projet gitlab avec un README présentant le projet et les maquettes (et à terme une explication de votre méthodologie pour le code).
Commentez votre code.

## Exemple : Qui suis-je ?
Retrouver le prénom de quelqu'un de la promo.

### Page 1 :
- Question 1 : Homme ou femme ?
- Question 2 : Portes-tu des lunettes ?
- Question 3 : As-tu plus de 35 ans ?

### Page 2 :
Première analyse des réponses, pour réduire la liste des possibilités. On pose alors des questions plus précises :
- Question 1 : As-tu les cheveux bruns ?
- Question 2 : As-tu des tâches de rousseur ?
- Question 3 : Écris-tu de la main gauche ?

### Page 3 :
Seconde analyse, qui affine encore plus la sélection finale. On lui demande enfin des questions de vérification :
- Question 1 : As-tu des enfants ?
- Question 2 : Viens-tu en tram le matin ?
- Question 3 : Ton prénom commence par un "S" ?

### Page de réponse :
On traite les dernières réponses, et on renvoie le résultat :
- On affiche le prénom de la personne.

On pourrait ajouter un côté "intelligence artificielle", en lui faisant donner la réponse avec hésitation :
afficher "J'hésite...", puis effacer et afficher "Sarah ou Sandy, j'hésite..." puis finalement afficher le bon prénom : "J'ai choisi : ton prénom est Sarah !".

