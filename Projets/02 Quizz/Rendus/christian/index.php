<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/svg+xml" />
  <title>Quiz en PHP - JS</title>
  <link rel="stylesheet" href="./assets/css/main.css" />
</head>

<body>
  <main>
    <div id="container">
      <header>
        <h1>Quiz Géographie</h1>
        <h2>Sur les capitales du&nbsp;monde</h2>
      </header>

      <button onclick="question_1();" class="start_game" id="start_game">
        <p>Les questions de&nbsp;1&nbsp;à&nbsp;5</p>
      </button>
      <button class="btnstyle" id="btnQuest2" onclick="question_2();">
        Aller à la question 2
      </button>
      <button class="btnstyle" id="btnQuest3" onclick="question_3();">
        Aller à la question 3
      </button>
      <button class="btnstyle" id="btnQuest4" onclick="question_4();">
        Aller à la question 4
      </button>
      <button class="btnstyle" id="btnQuest5" onclick="question_5();">
        Aller à la question 5
      </button>

      <form action="result.php" method="post" id="quiz">
        <ul id="urli">
          <li id="first_li">
            <h3>La capitale du Danemark est<span>...</span></h3>

            <div>
              <input type="radio" name="question-1" id="question-1-A" value="A" checked />
              <label for="question-1-A">A) Bruxelles</label>
            </div>

            <div>
              <input type="radio" name="question-1" id="question-1-B" value="B" />
              <label for="question-1-B">B) Praia</label>
            </div>

            <div>
              <input type="radio" name="question-1" id="question-1-C" value="C" />
              <label for="question-1-C">C) Copenhaque</label>
            </div>

            <div>
              <input type="radio" name="question-1" id="question-1-D" value="D" />
              <label for="question-1-D">D) Helsinki</label>
            </div>
          </li>

          <li id="second_li">
            <h3>La capitale de la Chine est<span>...</span></h3>

            <div>
              <input type="radio" name="question-2" id="question-2-A" value="A" checked />
              <label for="question-2-A">A) Honk Kong</label>
            </div>

            <div>
              <input type="radio" name="question-2" id="question-2-B" value="B" />
              <label for="question-2-B">B) Phnom Penh</label>
            </div>

            <div>
              <input type="radio" name="question-2" id="question-2-C" value="C" />
              <label for="question-2-C">C) Pyongyang</label>
            </div>

            <div>
              <input type="radio" name="question-2" id="question-2-D" value="D" />
              <label for="question-2-D">D) Pékin</label>
            </div>
          </li>

          <li id="third_li">
            <h3>La capitale de la Géorgie est<span>...</span></h3>

            <div>
              <input type="radio" name="question-3" id="question-3-A" value="A" checked />
              <label for="question-3-A">A) Tbilissi</label>
            </div>

            <div>
              <input type="radio" name="question-3" id="question-3-B" value="B" />
              <label for="question-3-B">B) Suva</label>
            </div>

            <div>
              <input type="radio" name="question-3" id="question-3-C" value="C" />
              <label for="question-3-C">C) Bakou</label>
            </div>

            <div>
              <input type="radio" name="question-3" id="question-3-D" value="D" />
              <label for="question-3-D">D) Palikir</label>
            </div>
          </li>

          <li id="fourth_li">
            <h3>La capitale du Brésil est<span>...</span></h3>

            <div>
              <input type="radio" name="question-4" id="question-4-A" value="A" checked />
              <label for="question-4-A">A) Bogota</label>
            </div>

            <div>
              <input type="radio" name="question-4" id="question-4-B" value="B" />
              <label for="question-4-B">B) Brasilia</label>
            </div>

            <div>
              <input type="radio" name="question-4" id="question-4-C" value="C" />
              <label for="question-4-C">C) S&atilde;o Paulo</label>
            </div>

            <div>
              <input type="radio" name="question-4" id="question-4-D" value="D" />
              <label for="question-4-D">D) Budapest</label>
            </div>
          </li>

          <li id="fifth_li">
            <h3>La capitale du Luxembourg est<span>...</span></h3>

            <div>
              <input type="radio" name="question-5" id="question-5-A" value="A" checked />
              <label for="question-5-A">A) Reykjavik</label>
            </div>

            <div>
              <input type="radio" name="question-5" id="question-5-B" value="B" />
              <label for="question-5-B">B) Port Louis</label>
            </div>

            <div>
              <input type="radio" name="question-5" id="question-5-C" value="C" />
              <label for="question-5-C">C) Castries</label>
            </div>

            <div>
              <input type="radio" name="question-5" id="question-5-D" value="D" />
              <label for="question-5-D">D) Luxembourg</label>
            </div>
          </li>
        </ul>

        <input type="submit" value="Confirmez" id="SUBMIT" />
      </form>
    </div>
  </main>
  <script src="./assets/js/script.js"></script>
</body>

</html>